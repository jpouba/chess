"""
    Chess, Very basic chess engine and UI
    Copyright (C) 2023  Jindrich Pouba (jindrich.pouba@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from engine import ng_white, \
    parse_move, parse_board_from_fen, \
    get_web_output, \
    get_best_moves, recursive_eval, gen_boards, \
    checkmate_evaluation_val

from flask import Flask
from flask import render_template, request

from chess import Board, Move

from requests_futures.sessions import FuturesSession
import json


app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template("home.html")


@app.route('/ng/white')
def page_ng_white():
    return render_template("chessboard.html", state=ng_white())


@app.route('/ng/black')
def page_ng_black():
    board = Board()
    bm = get_best_moves(board, rec_level=rec_level)[0]
    return render_template("chessboard.html", state=get_web_output(bm))


@app.route('/play')
def page_play():
    fen = request.args.get('fen')
    move = request.args.get('move')
    board = parse_board_from_fen(fen)
    return_if_checkmate = _check_for_checkmate(board, move)
    if return_if_checkmate: return return_if_checkmate

    bm = get_best_moves(board, rec_level=2)[0]
    return render_template("chessboard.html", state=get_web_output(bm))


@app.route('/async/play')
def async_page_play():
    fen = request.args.get('fen')
    move = request.args.get('move')
    board = parse_board_from_fen(fen)
    return_if_checkmate = _check_for_checkmate(board, move)
    if return_if_checkmate: return return_if_checkmate

    boards_and_futures = get_futures_for_next_moves_eval(board, 3)

    evaluation: int = checkmate_evaluation_val + 1
    best_board = None
    for pb, f in boards_and_futures:
        res = f.result()
        board_eval = json.loads(res.text).get('eval')
        if board_eval < evaluation:
            evaluation = board_eval
            best_board = pb
    return render_template("chessboard.html", state=get_web_output(best_board))


def _check_for_checkmate(board: Board, move: str):
    if board.is_checkmate():
        return render_template("chessboard.html", state=get_web_output(board))

    move = parse_move(move)
    board.push(move)
    if board.is_checkmate():
        return render_template("chessboard.html", state=get_web_output(board, last_move_played_by_engine=False))

    return None


@app.route('/eval')
def page_eval():
    fen = request.args.get('fen')
    rec_level = int(request.args.get('rec_level'))
    board = parse_board_from_fen(fen)
    evaluation = recursive_eval(board, rec_level=rec_level)
    return {'eval': evaluation}


@app.route('/async/eval')
def async_page_eval():
    fen = request.args.get('fen')
    rec_level = int(request.args.get('rec_level'))
    board = parse_board_from_fen(fen)
    boards_and_futures = get_futures_for_next_moves_eval(board, rec_level)

    evaluation: int = checkmate_evaluation_val
    for pb, f in boards_and_futures:
        res = f.result()
        board_eval = json.loads(res.text).get('eval')
        if board_eval < evaluation: evaluation = board_eval
    return {'eval': -evaluation}


def get_futures_for_next_moves_eval(board, rec_level):
    possible_next_boards = gen_boards(board)
    session = FuturesSession()
    boards_and_futures = list()
    for pb in possible_next_boards:
        f = session.get(f'http://127.0.0.1:5000/eval?fen={pb.fen()}&rec_level={rec_level - 1}')
        boards_and_futures.append([pb, f])
    return boards_and_futures


if __name__ == '__main__':
    app.run()
