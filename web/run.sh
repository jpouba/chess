#!/bin/bash

VERSION=$1

docker kill chess_web
docker rm chess_web
docker run -d --name chess_web -p 5000:5000 registry.gitlab.com/jpouba/chess:$VERSION
