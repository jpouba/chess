import chess
from chess import Board


def ng_white():
    board = Board()
    return get_web_output(board)


def get_web_output(board: Board, last_move_played_by_engine: bool = True) -> dict:
    arr_board = list()
    for i in range(8):
        arr_board.append([None, None, None, None, None, None, None, None])

    for k, v in board.piece_map().items():
        arr_board[chess.square_rank(k)][chess.square_file(k)] = v.symbol()

    checkmate = None
    moves = list()
    if board.is_checkmate():
        checkmate = "ENGINE" if last_move_played_by_engine else "PLAYER"
    else:
        for lm in board.legal_moves:
            moves.append([lm.uci(), board.san(lm)])

    return { 'fen': board.fen(), 'arr_board': arr_board, 'moves': moves, 'checkmate': checkmate}
