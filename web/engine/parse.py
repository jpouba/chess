from chess import Board, Move


def parse_board_from_fen(fen: str) -> Board:
    return Board(fen=fen)


def parse_move(uci: str) -> Move:
    return Move.from_uci(uci)
