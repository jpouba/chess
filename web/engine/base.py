import chess

from chess import Board, SquareSet, Color
from typing import List

checkmate_evaluation_val: int = 1000000000


def gen_boards(board: Board):
    ret = list()
    moves = board.legal_moves
    for m in moves:
        nb = board.copy()
        nb.push(m)
        ret.append(nb)
    return ret


def eval_board(board: Board) -> int:
    piece_val = get_piece_val_sum(board, color=board.turn)
    piece_val_opponent = get_piece_val_sum(board, color=not board.turn)
    piece_val_diff = piece_val - piece_val_opponent

    attacking_sqrs = get_attacked_sqrs(board, color=board.turn)
    attacking_sqrs_opponent = get_attacked_sqrs(board, color=not board.turn)
    attacking_sqrs_diff = len(attacking_sqrs) - len(attacking_sqrs_opponent)

    attacking_pieces_sum = get_attacked_sqrs_piece_vals(board, color=board.turn)
    attacking_pieces_sum_opponent = get_attacked_sqrs_piece_vals(board, color=not board.turn)
    attacking_pieces_sum_diff = attacking_pieces_sum - attacking_pieces_sum_opponent

    defended_pieces_sum = get_defended_sqrs_piece_vals(board, color=board.turn)
    defended_pieces_sum_opponent = get_defended_sqrs_piece_vals(board, color=not board.turn)
    defended_pieces_sum_diff = defended_pieces_sum - defended_pieces_sum_opponent

    return piece_val_diff*1000 \
        + attacking_sqrs_diff*1 \
        + attacking_pieces_sum_diff*5 \
        + defended_pieces_sum_diff*2


def get_all_pieces(board: Board, color: Color) -> SquareSet:
    ret = SquareSet()
    for t in [chess.KING, chess.QUEEN, chess.ROOK, chess.KNIGHT, chess.BISHOP, chess.PAWN]:
        ret = ret | board.pieces(piece_type=t, color=color)
    return ret


def get_piece_val(piece_type: chess.PieceType) -> int:
    if piece_type == chess.KING: return 20
    if piece_type == chess.QUEEN: return 9
    if piece_type == chess.ROOK: return 5
    if piece_type == chess.BISHOP: return 3
    if piece_type == chess.KNIGHT: return 3
    if piece_type == chess.PAWN: return 1


def get_piece_val_sum(board: Board, color: Color) -> int:
    ret = 0
    for t in [chess.QUEEN, chess.ROOK, chess.KNIGHT, chess.BISHOP, chess.PAWN]:
        count = len(board.pieces(piece_type=t, color=color))
        ret = ret + count * get_piece_val(t)
    return ret


def get_attacked_sqrs(board: Board, color: Color) -> SquareSet:
    sqrs = SquareSet()
    pieces = get_all_pieces(board, color)
    for p in pieces:
        sqrs = sqrs | board.attacks(p)
    return sqrs


def get_attacked_sqrs_piece_vals(board: Board, color: Color) -> int:
    ret = 0
    attacked_sqrs = get_attacked_sqrs(board, color)
    for atsq in attacked_sqrs:
        p = board.piece_at(atsq)
        if p is not None and p.color is not color:
            ret = ret + get_piece_val(p.piece_type)
    return ret


def get_defended_sqrs_piece_vals(board: Board, color: Color) -> int:
    ret = 0
    attacked_sqrs = get_attacked_sqrs(board, color)
    for atsq in attacked_sqrs:
        p = board.piece_at(atsq)
        if p is not None and p.color is board.turn and p.piece_type is not chess.KING:
            ret = ret + get_piece_val(p.piece_type)
    return ret


def recursive_eval(board: Board, rec_level: int = 0, stats: dict = dict()) -> int:
    if stats is not None:
        count_for_lvl = stats.get(rec_level)
        if count_for_lvl is None: count_for_lvl = 1
        else: count_for_lvl = count_for_lvl + 1
        stats[rec_level] = count_for_lvl
    if board.is_checkmate(): return -checkmate_evaluation_val
    if rec_level == 0: return eval_board(board)
    best_eval: int = checkmate_evaluation_val
    for m in gen_boards(board):
        move_eval = recursive_eval(m, rec_level=rec_level - 1, stats=stats)
        if best_eval is None or move_eval < best_eval: best_eval = move_eval
    return -best_eval


def get_best_moves(board: Board, rec_level: int = 0, num_moves: int = 1) -> List[Board]:
    st = dict()
    next_moves = list()
    for b in gen_boards(board):
        next_moves.append([b, recursive_eval(b, rec_level=rec_level, stats=st)])
    next_moves.sort(key=lambda mv: mv[1])
    ret = list()
    for nm in next_moves[:num_moves]:
        ret.append(nm[0])
    return ret
