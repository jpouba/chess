#!/bin/bash

VERSION=$1

docker build -t registry.gitlab.com/jpouba/chess:$VERSION -f ./docker/Dockerfile .
docker push registry.gitlab.com/jpouba/chess:$VERSION
