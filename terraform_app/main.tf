provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "minikube"
}

resource "kubernetes_namespace" "chess" {
  metadata {
    name = "chess"
  }
}

resource "kubernetes_deployment" "chess" {
  metadata {
    name = "chess"
    namespace = kubernetes_namespace.chess.metadata[0].name
    labels = {
      app = "chess"
    }
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "chess"
      }
    }

    template {
      metadata {
        labels = {
          app = "chess"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/jpouba/chess:v0.0.2"
          name  = "web"

          port {
            container_port = 5000
          }

          resources {
            limits = {
              cpu    = "1"
              memory = "512Mi"
            }
            requests = {
              cpu    = "100m"
              memory = "50Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 5000
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "chess" {
  metadata {
    name = "chess"
    namespace = kubernetes_namespace.chess.metadata[0].name
  }
  spec {
    selector = {
      app = kubernetes_deployment.chess.spec[0].template[0].metadata[0].labels.app
    }
    port {
      port        = 5000
      target_port = 5000
      node_port   = 30000
    }

    type = "NodePort"
  }
}
