terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
}

resource "aws_key_pair" "main" {
  key_name   = "Main"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDxqDFQ4y3ULWEmW3BXa7EJQwc9sIxVGgaw7m5IOUsr54qrAptgOUCGzF8qSeC8a4BCfmTIpj1d4LtNp3o4brEz7BXD9Ty0BPhql4gBsODj5+XW5exIA3KtGLdGZu2sIpi9h04J//jLWj/NQIdiNwamhAkxSaybHVh3kF6NWhxi7s8cCn0VuCwkSmcvNcutqww/F1mTp0ROXjqeoDXW3j5/AtYPgu/yFw9z25aYzT+o6grAcwDuPWIJdmWsdyK3hs3LIdYSQs3mgAhYX89Hno24FStiQPdJJ0iUS3FrxnSNNMdTV7CnubkbH/3rS95+WhLTn7SyNB6CpW5ol3xrznc7vcFuVajeJGj2WZIwhDR/+zFYn5oHub/BI2muH7BtdJyQ0l/jQh6EjfejMfnq2p5cN58XK8DS4jz90K5CGCyPZA8oEsgjzxsWEoQmYZ5kyKsu3XShoz+XadOMThL/uiaR38TLVqC5WZL3xgbaFEcrCjlwdx3ChwUkJXEl5CETGh8= jpouba@jpouba-pc"
}


resource "aws_instance" "chess_web" {
  ami           = "ami-0039da1f3917fa8e3"
  instance_type = "t2.medium"

  tags = {
    Name = "ChessWeb"
  }

  key_name = aws_key_pair.main.key_name

  # user_data = file("templates/startup.sh")
  security_groups = ["SSH access"]
}